$(document).ready(function (e) {
  $(window).on("scroll", function () {
    if ($(this).scrollTop() > 20) {
      $(".header").addClass("fixed");
    } else {
      $(".header").removeClass("fixed");
    }
  });

  $(".header a, .start-project").on("click", function (event) {
    let hash = this.hash;
    if (hash !== "") {
      event.preventDefault();

      $("html, body")
        .stop()
        .animate(
          {
            scrollTop: $(hash).offset().top - 100,
          },
          800
        );
    }
  });

  // $('input[name="phone"]').inputmask({ mask: "+(999) 999-9999" }); //specifying options

  var input = document.querySelector("#phone");
  window.intlTelInput(input, {
    onlyCountries: [
      "al",
      "at",
      "by",
      "be",
      "hr",
      "dk",
      "fr",
      "de",
      "gr",
      "hu",
      "is",
      "ie",
      "in",
      "it",
      "kz",
      "nl",
      "no",
      "pl",
      "pt",
      "ro",
      "ru",
      "es",
      "se",
      "ch",
      "ua",
      "gb",
      "us",
      "uz",
    ],
    autoPlaceholder: "aggressive",
    preferredCountries: ["us", "uz"],
    // separateDialCode: true,
    utilsScript: "utils.js?1613236686837",
  });

  $(".start-project").on("click", function () {
    $('input[name="name"]').focus();
  });

  $(".banner-slider").owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: true,
    smartSpeed: 600,
    responsive: {
      0: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  });

  var swiper = new Swiper(".review-slider", {
    direction: "vertical",
    autoHeight: true,
    spaceBetween: 20,
    slidesPerView: 3,
    autoHeight: true,
    grabCursor: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      type: "bullets",
    },
  });

  // TAB TECHNOLOGIES AND TOOLS

  // Toogle all elements except All button

  let AllTabBtn = $(".technology-tabPickerItemAll");
  let TabBtn = $(".technology-tabPickerItem");

  TabBtn.on("click", function () {
    AllTabBtn.removeClass("technology-tabPickerItem--active");

    $(this).toggleClass("technology-tabPickerItem--active");

    if (
      $(this).hasClass("technology-tabPickerItem--active") &&
      !$(this).siblings().hasClass("technology-tabPickerItem--active")
    ) {
      $(this).parent().parent().find(".technology-tabBodyItem").hide();
      $(this)
        .parent()
        .parent()
        .find(".technology-tabBodyItem")
        .eq($(this).index())
        .fadeIn(200);
    } else if ($(this).hasClass("technology-tabPickerItem--active")) {
      $(this)
        .parent()
        .parent()
        .find(".technology-tabBodyItem")
        .eq($(this).index())
        .fadeIn(200);
    } else if (
      !$(this).hasClass("technology-tabPickerItem--active") &&
      !$(this).siblings().hasClass("technology-tabPickerItem--active")
    ) {
      $(".technology-tabPickerItemAll").addClass(
        "technology-tabPickerItem--active"
      );
      $(this).parent().parent().find(".technology-tabBodyItem").fadeIn(200);
    } else {
      $(this)
        .parent()
        .parent()
        .find(".technology-tabBodyItem")
        .eq($(this).index())
        .hide();
    }
  });

  // All button
  AllTabBtn.on("click", function () {
    if ($(this).hasClass("technology-tabPickerItem--active")) {
      $(this).parent().parent().find(".technology-tabBodyItem").show();
    } else {
      $(this).parent().parent().find(".technology-tabBodyItem").hide();
      $(this).parent().parent().find(".technology-tabBodyItem").fadeIn(200);
      $(this).addClass("technology-tabPickerItem--active");
      $(this).siblings().removeClass("technology-tabPickerItem--active");
    }
  });

  // ANIMATE ELEMENTS

  // 1 el
  $(".service-item:nth-child(1)").on("mouseenter", function () {
    $(".service-item:nth-child(3)").addClass("smaller");
    $(this).addClass("larger");
  });

  $(".service-item:nth-child(1)").on("mouseleave", function () {
    $(".service-item:nth-child(3)").removeClass("smaller");
    $(this).removeClass("larger");
  });

  // 2 el
  $(".service-item:nth-child(2)").on("mouseenter", function () {
    $(".service-item:nth-child(1)").addClass("smaller");
    $(this).addClass("larger");
  });

  $(".service-item:nth-child(2)").on("mouseleave", function () {
    $(".service-item:nth-child(1)").removeClass("smaller");
    $(this).removeClass("larger");
  });

  // 3 el
  $(".service-item:nth-child(3)").on("mouseenter", function () {
    $(".service-item:nth-child(2)").addClass("smaller");
    $(this).addClass("larger");
  });
  $(".service-item:nth-child(3)").on("mouseleave", function () {
    $(".service-item:nth-child(2)").removeClass("smaller");
    $(this).removeClass("larger");
  });

  // 4 el
  $(".service-item:nth-child(4)").on("mouseenter", function () {
    $(".service-item:nth-child(6)").addClass("smaller");
    $(this).addClass("larger");
  });
  $(".service-item:nth-child(4)").on("mouseleave", function () {
    $(".service-item:nth-child(6)").removeClass("smaller");
    $(this).removeClass("larger");
  });

  // 5 el
  $(".service-item:nth-child(5)").on("mouseenter", function () {
    $(".service-item:nth-child(4)").addClass("smaller");
    $(this).addClass("larger");
  });
  $(".service-item:nth-child(5)").on("mouseleave", function () {
    $(".service-item:nth-child(4)").removeClass("smaller");
    $(this).removeClass("larger");
  });

  // 6 el
  $(".service-item:nth-child(6)").on("mouseenter", function () {
    $(".service-item:nth-child(5)").addClass("smaller");
    $(this).addClass("larger");
  });
  $(".service-item:nth-child(6)").on("mouseleave", function () {
    $(".service-item:nth-child(5)").removeClass("smaller");
    $(this).removeClass("larger");
  });

  // COPYRIGHT YEAR
  let currentYear = new Date().getFullYear();
  $(".currentYear").text(currentYear);
});
